package algorithms;

import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

import game.State;
import game.StateComparator;

public class AStar extends Algorithm {
	private static final int INITIAL_QUEUE_CAPACITY = 10000;
	
	public AStar() {
		super();
	}
	
	public AStar(State initialState, State goalState) {
		super(initialState, goalState);
	}
	
	public AStar(AStar algorithm) {
		super(algorithm);
	}

	@Override
    public Solution solve() {
		long timeStart = System.nanoTime();
        int nodeExplored = 0;
        StatePriorityQueue openSet = new StatePriorityQueue();
        Set<State> closeSet = new HashSet<State>();

        openSet.add(initialState);
        closeSet.add(initialState);
        while (!openSet.isEmpty()) {
            nodeExplored++;
            
            State current = openSet.poll();
            if (current.equals(goalState)) {
	            	String message = String.format("A Star Solution with heuristic %s", current.getHeuristic().toString());
                 return new Solution(message, nodeExplored, current.getDepth(), System.nanoTime() - timeStart, current.getPath());
            }
            List<State> neighbors = current.generateNeighbors();
            for (State state : neighbors) {
            	if(closeSet.contains(state)) {
            		continue;
            	}
                if(openSet.contains(state)) {
                	State oldState = openSet.getState(state);
                	if(state.getScore() < oldState.getScore()) {
                		openSet.remove(oldState);
                		openSet.add(state);
                	}
                }
                else {
                	openSet.add(state);
                }
                	
           }
            closeSet.add(current);
         }
        throw new IllegalStateException("No route found");
    }
	
	class StatePriorityQueue {
		private PriorityQueue<State> openSet;
		private Set<State> setInQueue; 
		
		public StatePriorityQueue() {
			this.openSet = new PriorityQueue<State>(INITIAL_QUEUE_CAPACITY,  new StateComparator());
			this.setInQueue = new HashSet<State>();
		}
		
		public boolean add(State state) {
			return openSet.add(state) | setInQueue.add(state);
		}
		
		public boolean remove(State state) {
			return openSet.remove(state) | setInQueue.remove(state);
		}
		
		public State poll() {
			State state = openSet.poll();
			setInQueue.remove(state);
			return state;
		}
		
		public boolean isEmpty() {
			return this.openSet.isEmpty();
		}
		
		public boolean contains(State state) {
			return this.openSet.contains(state);
		}
		
		public State getState(State state) {
			for (State s : this.setInQueue) {
				if(s.equals(state)) {
					return s;
				}
			}
			return null;
		}
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return new AStar();
	}

	@Override
	public String toString() {
		return String.format("AStar\n%s", super.toString());
	}
}
