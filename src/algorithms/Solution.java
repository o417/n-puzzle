package algorithms;

import java.util.Stack;

import game.State;

public class Solution {
	private int nodeExplored;
	private int numOfSteps;
	private long nanoDuration;
	private Stack<State> path;
	private String message;

	public Solution(String message, int nodeExplored, int numOfSteps, long nanoDuration, Stack<State> path) {
		this.message = message;
		this.nodeExplored = nodeExplored;
		this.numOfSteps = numOfSteps;
		this.nanoDuration = nanoDuration;
		this.path = path;
	}

	public Solution() {
		this(null, 0, 0, 0, null);
	}

	public int getNodeExplored() {
		return nodeExplored;
	}

	public void setNodeExplored(int nodeExplored) {
		this.nodeExplored = nodeExplored;
	}

	public int getNumOfSteps() {
		return numOfSteps;
	}

	public void setNumOfSteps(int numOfSteps) {
		this.numOfSteps = numOfSteps;
	}

	public long getNanoDuration() {
		return nanoDuration;
	}

	public void setNanoDuration(long nanoDuration) {
		this.nanoDuration = nanoDuration;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public Stack<State> getPath() {
		return path;
	}

	@Override
	public String toString() {
		return String.format("%s\nnodeExplored: %d, numOfSteps: %d, nanoDuration: %d",
				message, nodeExplored, numOfSteps, nanoDuration);
	}
}
