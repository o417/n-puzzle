package algorithms;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import game.State;

public class BreathFirstSearch extends Algorithm {

	public BreathFirstSearch() {
        super();
    }
	 public BreathFirstSearch(State initialState, State goalState) {
		 super(initialState, goalState);
	}

	 public BreathFirstSearch(BreathFirstSearch algorithm) {
		 this(algorithm.initialState, algorithm.goalState);
	}
	 
	    @Override
	    public Solution solve() {
	    	long timeStart = System.nanoTime();
	        int nodeExplored = 0;
	        Queue<State> queue = new LinkedList<State>();
	        Set<State> visited = new HashSet<State>();

	        queue.add(initialState);
	        visited.add(initialState);
	        while (!queue.isEmpty()) {
	            nodeExplored++;
	            State current = queue.poll();
	            if (current.equals(goalState)) {
	            	String message = "BFS Solution";
	                 return new Solution(message, nodeExplored, current.getDepth(), System.nanoTime() - timeStart, current.getPath());
	            }
	
	            List<State> nextStates = current.generateNeighbors();
	            for (State state : nextStates) {
	                if (!visited.contains(state)){
	                    queue.add(state);
	                    visited.add(state);
	                }
	            }
	        }
	        throw new IllegalStateException("No route found");
	    }
	@Override
	public Object clone() throws CloneNotSupportedException {
		return new BreathFirstSearch();
	}
	@Override
	public String toString() {
		return String.format("BFS\n%s", super.toString());
	}
}
