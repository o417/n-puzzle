package algorithms;

import game.State;

public abstract class Algorithm implements Cloneable {
	protected State initialState;
	protected State goalState;

	public Algorithm() {
		this.initialState = null;
		this.goalState = null;
	}
	
	public Algorithm(State initialState, State goalState) {
		this.initialState = new State(initialState);
		this.goalState = new State(goalState);
	}
	
	public Algorithm(Algorithm algorithm) {
		this(algorithm.getInitialState(), algorithm.getGoalState());
	}
	    
	public abstract Solution solve();
	
	public State getInitialState() {
		return initialState;
	}

	public void setInitialState(State initialState) {
		this.initialState = initialState;
	}
	
	public State getGoalState() {
		return goalState;
	}

	public void setGoalState(State goalState) {
		this.goalState = goalState;
	}
	
    abstract public Object clone() throws CloneNotSupportedException;
    public String toString() {
    	return String.format("Intial State\n%sGoal State\n%s", this.initialState, this.goalState);
    }

}
