package exceptions;

public class IllegalStateException extends Exception{
	
	public IllegalStateException(String errorMessage) {
		super(errorMessage);
	}
}
