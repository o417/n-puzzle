package exceptions;

public class ChoiceException extends Exception{
	
	public ChoiceException(String errorMessage) {
		super(errorMessage);
	}
}
