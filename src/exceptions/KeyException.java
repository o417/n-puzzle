package exceptions;

public class KeyException extends Exception{
	public KeyException(String errorMessage) {
		super(errorMessage);
	}
}