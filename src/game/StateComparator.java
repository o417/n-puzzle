package game;

import java.util.Comparator;

public class StateComparator implements Comparator<State> {

    @Override
    public int compare(State s1, State s2) {
        if (s1.getScore() < s2.getScore()){
        	return -1;
        } else if (s1.getScore() > s2.getScore()) {
        	return 1;
        }
        return 0;
    }
}
	