package game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Board {
	private int rows;
    private int columns;
	private int[][] board;
	private Cell emptyCell;
	public static final int EMPTY_CELL_VALUE = 0;
	
	enum Move {
		RIGHT, LEFT, UP, DOWN, NO_LAST_MOVE
	};
	
	Move lastMove = Move.NO_LAST_MOVE;

	private Board(int[][] board, Move lastMove) {
		setRows(board.length);
		setColumns(board[0].length);
		this.board = new int[this.rows][this.columns];
		this.emptyCell = new Cell();
		setBoard(board);
		this.lastMove = lastMove;
	}

	public Board(int[][] board) {
		this(board, Move.NO_LAST_MOVE);
	}
	 
	public Board(Board source) 
	{
		this(source.board, source.lastMove);
	}
	 
	private void setBoard(int[][] board) {
		// TODO: this method do 2 things
		for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
            	this.board[i][j] = board[i][j];
                if (board[i][j] == 0) {
                	this.emptyCell.setX(i);
                	this.emptyCell.setY(j);
                }
            }
		 }
	}
	
	 public int getRows() {
	        return rows;
	 }

	 private void setRows(int rows) {
	    	this.rows = rows;
	 }

	 public int getColumns() {
	        return columns;
	 }

	 private void setColumns(int columns) {
	    	this.columns = columns;
	 }
	
    public int[][] getBoard() {
        return board;
    }
    
	public Move getLastMove() {
		return lastMove;
	}
	public Cell getEmptyCell() {
		return emptyCell;
	}

	public int[] getFlattenBoard(){
		int[] flattenBoard = new int[this.rows*this.columns];
		for(int i = 0 ; i < this.rows; i++){
			for(int j = 0 ; j < this.columns; j++) {
				flattenBoard[this.columns * i + j] = this.board[i][j];
			}
		}
		return flattenBoard;
	}

    
    public static Board generateRandomBoard(int rows, int columns, int numMoves) {
    	Board board = getGoalBoard(rows, columns);
    	Random rand = new Random();
    	int numOptions;
    	
    	for (int i = 0; i < numMoves; i++) {
    		List<Board> newBoards = board.generateNextBoards();
    		numOptions = newBoards.size();
    		int move = rand.nextInt(numOptions);
    		board = newBoards.get(move);
    	}
    	board.lastMove = Move.NO_LAST_MOVE;
    	return board;
    }
    
    public List<Board> generateNextBoards() {
        List<Board> nextBoards = new ArrayList<>();
        // The movements are from the empty cell point of view
        if (checkRight()){
        	nextBoards.add(moveRight());
        }
        if (checkDown()) {
        	nextBoards.add(moveDown());
        }
        if (checkUp()) {
        	nextBoards.add(moveUp());
        }
        if (checkLeft()) {
        	nextBoards.add(moveLeft());
        }

        return nextBoards;
    }
    
    private boolean checkLeft() {
        int pos = this.emptyCell.getY() - 1;
        if (pos < 0)
            return false;
        return true;
    }

    private boolean checkRight() {
        int pos = this.emptyCell.getY() + 1;
        if (pos >= rows)
            return false;
        return true;
    }

    private boolean checkUp() {
    	int pos = this.emptyCell.getX() - 1;
        if (pos < 0)
            return false;
        return true;
    }

    private boolean checkDown() {
    	int pos = this.emptyCell.getX() + 1;
        if (pos == this.columns)
            return false;
        return true;
    }
    
    private Board moveLeft() {
    	Board newBoard = new Board(this.getBoard());
    	int x = emptyCell.getX();
    	int y = emptyCell.getY();
    	
        newBoard.board[x][y] = newBoard.board[x][y-1];
        newBoard.board[x][y-1] = EMPTY_CELL_VALUE;
        newBoard.emptyCell.setY(y-1);
		// Left to the empty cell is right
        newBoard.lastMove = Move.RIGHT;
        return newBoard;
    }

    private Board moveRight() {
    	Board newBoard = new Board(this.getBoard());
    	int x = emptyCell.getX();
    	int y = emptyCell.getY();
    	
        newBoard.board[x][y] = newBoard.board[x][y+1];
        newBoard.board[x][y+1] = EMPTY_CELL_VALUE;
        newBoard.emptyCell.setY(y+1);
		// Right to the empty cell is left
        newBoard.lastMove = Move.LEFT;
        return newBoard;
    }

    private Board moveUp() {
    	Board newBoard = new Board(this.getBoard());
    	int x = emptyCell.getX();
    	int y = emptyCell.getY();
    	
        newBoard.board[x][y] = newBoard.board[x-1][y];
        newBoard.board[x-1][y] = EMPTY_CELL_VALUE;
        newBoard.emptyCell.setX(x-1);
		// Up to the empty cell is down
        newBoard.lastMove = Move.DOWN;
        return newBoard; 
    }
    
    private Board moveDown() {
    	Board newBoard = new Board(this.getBoard());
    	int x = emptyCell.getX();
    	int y = emptyCell.getY();
    	
        newBoard.board[x][y] = newBoard.board[x+1][y];
        newBoard.board[x+1][y] = EMPTY_CELL_VALUE;
        newBoard.emptyCell.setX(x+1);
		// Down to the empty cell is up
        newBoard.lastMove = Move.UP;
        return newBoard; 
    }
    
    @Override
    public int hashCode() {
        int hash = 17;
        hash = 31 * hash + Arrays.deepHashCode(board);
        return hash;
    }

    @Override
    public boolean equals(Object o) {
		if(o == null){
			return false;
		}

        else if (!(o instanceof Board)) {
        	return false;
        }
        Board other = (Board) o;
        
        if(this.rows != other.rows || this.columns != other.columns) {
        	return false;
        }
        
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                if (this.board[i][j] != other.board[i][j])
                {
                    return false;
                }
            }
        }
        return true;
    }
    
    public static Board getGoalBoard(int rows, int columns) {
    	int [][] board = new int[rows][columns];
    	int num = 1;
    	
    	for(int i = 0; i< rows; i++) {
    		for(int j = 0 ; j < columns; j++) {
    			board[i][j] = num;
    			num += 1;
    		}
    	}
    	board[rows-1][columns-1] = EMPTY_CELL_VALUE;
    	return new Board(board);
    }

    public boolean isInGoalPosition(int value){
		Cell valueGoalPosition = getValueGoalPosition(value, rows, columns);
		int x = valueGoalPosition.getX();
		int y = valueGoalPosition.getY();
		return value == this.board[x][y];
	}

	public static boolean isValueValid(int value, int rows, int columns){
		return value >= getMinValidValue() || value <= getMaxValidValue(rows, columns);
	}

    public static Cell getValueGoalPosition(int value, int rows, int columns) {
    	if(!isValueValid(value, rows, columns)){
			return null;
		}
		int goalRow = (value - 1) / rows;
		int goalCol = (value - 1) % columns;
		return new Cell(goalRow, goalCol);
    }

	public static int getMaxValidValue(int rows, int columns){
		return rows*columns-1;
	}

	public static int getMinValidValue(){
		return 0;
	}

	public static List<Integer> getValidValues(int rows, int columns) {
		List<Integer> validValues = new ArrayList<Integer>();
		int maxValidValue = getMaxValidValue(rows, columns);
		int minValidValue = getMinValidValue(); 
		for(int i = minValidValue; i <= maxValidValue; i++){
			validValues.add(i);
		}
		return validValues;
	}

    @Override
    public String toString() {
    	StringBuilder s = new StringBuilder();
    	s.append(this.lastMove+"\n");
    	for (int i = 0; i < board.length; i++) {
    		s.append(Arrays.toString(this.board[i]));
    		s.append("\n");
    	}
    	return s.substring(0, s.length()-1).toString();
    }
  }
