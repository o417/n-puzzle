package game;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Stack;

import heuristic.Heuristic;
import heuristic.Zero;

public class State {
    private Board board;
    private State parent;
    private int depth;              //g function value - number of moves
    private Heuristic heuristic;
	private int heuristicValue;     // h function value

    public State(Board board, State parent, Heuristic heuristic) {
    	this.board = new Board(board);
        this.parent = parent;
        this.heuristic = heuristic;
        setScore();
    }

    public State(Board board, State parent) {
    	this(board, parent, new Zero());
    }

    public State(State source, Heuristic heuristic) {
        this(source.getBoard(), source.getParent(), heuristic);
    }
    
    public State(State source) {
    	this(source.getBoard(), source.getParent(), source.heuristic);
    }
    
    public void setDepth() {
        if (parent == null)
            this.depth = 0;
        else
            this.depth = parent.getDepth() + 1;
    }
    
    public int getDepth() {
        return this.depth;
    }
    
    public State getParent() {
        return this.parent;
    }
    
    public Board getBoard() {
    	return this.board;
    }
    
    public Heuristic getHeuristic() {
		return heuristic;
	}

    public void setHeuristicValue() {
        this.heuristicValue = heuristic.getHeuristicValue(this);
    }
    
    public int getScore() {
    	// f = h + g
        return heuristicValue + depth;
    }

    public void setScore() {
    	setHeuristicValue();
        setDepth();
    }
    
    public Stack<State> getPath() {
        State current = this;
        Stack<State> path = new Stack<State>();
        while (current != null) {
            path.push(current);
            current = current.getParent();
        }
        return path;
    }
    
    public List<State> generateNeighbors() {
    	
    	List<Board> nextBoards = this.board.generateNextBoards();
        List<State> neighbors = new ArrayList<>();
        
        for(Board nextBoard : nextBoards) {
        	neighbors.add(new State(nextBoard, this, this.heuristic));
        }
        return neighbors;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.board);
    }
    
    @Override
    public boolean equals(Object o) {
        if(o == null){
            return false;
        }
        if (!(o instanceof State)){
            return false;
        }
        State other = (State) o;
        if (Objects.equals(this.board, other.board)) {
        	return true;
        }
        return false;
    }

    @Override
    public String toString() {
    	return String.format("Depth: %d, Heuristic Value: %d\n%s", this.depth, this.heuristicValue, this.board.toString());
    }

    public static State getGoalState(State state) {
    	return State.getGoalState(state.getBoard().getRows(), state.getBoard().getColumns(), state.heuristic);
    }
    
    public static State getGoalState(int rows, int columns, Heuristic heuristic ) {
    	return new State(Board.getGoalBoard(rows, columns), null, heuristic);
    }
    

    public static String getPath(Stack<State> path) {
    	StringBuilder s = new StringBuilder();
    	String buffer = "\n      |\n      V\n";
    	while(!path.isEmpty()) {
    		s.append(path.pop());
    		s.append(buffer);
    	}
    	return s.substring(0, s.length() - buffer.length() + 1).toString();
    }
}
