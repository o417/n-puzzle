package factories;

import exceptions.KeyException;

public interface IFactory <K, V> {
	 void InsertProduct(K key, V value);
     V GetNewProduct(K key) throws KeyException;
     boolean isProductKeyExists(K key);
}
