package factories;

import java.util.HashMap;
import java.util.Map;

import exceptions.KeyException;
import heuristic.Hamming;
import heuristic.Heuristic;
import heuristic.Manhatten;
import heuristic.InadmissibleHeuristic;
import heuristic.OppositeHamming;
import heuristic.Zero;

public class HeuristicFactory implements IFactory<String, Heuristic>{
	private Map<String, Heuristic> factory;
	
	public HeuristicFactory() {
		this.factory = new HashMap<String, Heuristic>();
		this.InsertProduct("Zero", new Zero());
		this.InsertProduct("Manhatten", new Manhatten());
		this.InsertProduct("Hamming", new Hamming());
		this.InsertProduct("OppositeHamming", new OppositeHamming());
		this.InsertProduct("InadmissibleHeuristic", new InadmissibleHeuristic());
	}

	@Override
	public void InsertProduct(String key, Heuristic value) {
		if(!this.factory.containsKey(key)) {
            this.factory.put(key, value);
		}
	}

	@Override
	public Heuristic GetNewProduct(String key) throws KeyException {
		 if (isProductKeyExists(key))
         {
            try {
				return (Heuristic) this.factory.get(key).clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
         }
		 throw new KeyException(String.format("HeuristicFactory - No such heuristic function product with key \'%s\'", key));
	}

	@Override
	public boolean isProductKeyExists(String key) {
		return this.factory.containsKey(key);
	}
}
