package factories;

import java.util.HashMap;
import java.util.Map;

import algorithms.AStar;
import algorithms.Algorithm;
import algorithms.BreathFirstSearch;
import exceptions.KeyException;

public class AlgorithmFactory implements IFactory<String, Algorithm>{
	
	Map<String, Algorithm> factory;
	
	public AlgorithmFactory() {
		this.factory = new HashMap<String, Algorithm>();
		this.InsertProduct("BFS", new BreathFirstSearch());
		this.InsertProduct("AStar", new AStar());
	}

	@Override
	public void InsertProduct(String key, Algorithm value) {
		if(!this.factory.containsKey(key)) {
            this.factory.put(key, value);
		}
	}

	@Override
	public Algorithm GetNewProduct(String key) throws KeyException{
		 if (isProductKeyExists(key))
         {
            try {
				return (Algorithm) this.factory.get(key).clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
         }
		 throw new KeyException(String.format("AlgorithmFactory - No such algorithm product with key \'%s\'", key));
	}

	@Override
	public boolean isProductKeyExists(String key) {
		return this.factory.containsKey(key);
	}
}
