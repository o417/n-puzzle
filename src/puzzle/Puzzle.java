package puzzle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import algorithms.Solution;
import exceptions.KeyException;
import game.Board;

public class Puzzle {
	public static Map<String, HashSet<String>> algorithmsCombinations;
 
	static {
		algorithmsCombinations = new HashMap<String, HashSet<String>>();
		algorithmsCombinations.put("BFS", new HashSet<String>
			(Arrays.asList("Zero")));
		algorithmsCombinations.put("AStar", new HashSet<String>
			(Arrays.asList("Zero","Manhatten", "InadmissibleHeuristic")));
	}

	public static int getNumAlgorithms(){
		int numAlgotithms = 0;
		for(HashSet<String> set : algorithmsCombinations.values()){
			numAlgotithms += set.size();
		}
		return numAlgotithms;
	}

	public static Map<String, Solution> runAllAlgorithms(Board board) throws KeyException {
		/**
		* This run all algorithms on a given board
		* @return  A List of solutions for each algorithm combination of algorithm and heuristic function
		*/

		Map<String, Solution> algorithmsSolutions = new HashMap<String, Solution>();


		for(Map.Entry<String, HashSet<String>> algorithmEntry: algorithmsCombinations.entrySet()){
			String algorithmName = algorithmEntry.getKey();
			for(String heuristicName : algorithmEntry.getValue()){
				String algoCombinationName = String.format("%s %s", algorithmName, heuristicName);
				algorithmsSolutions.put(algoCombinationName, runAlgorithm(board, algorithmName, heuristicName));
			}
		}

		return algorithmsSolutions;
	}
	
	public static Solution runAlgorithm(Board board, String algorithmName, String heuristicName) throws KeyException {
		PuzzleSolver solver = new PuzzleSolver.PuzzleSolverBuilder().setBoard(board)
				.setAlgorithmName(algorithmName).setHeuristicName(heuristicName).build();
		Solution solution = solver.solve();
		return solution;
	}
	
	public static Map<String, Solution> runComparator(int boardSize, int numBoards, int numRandomMoves) throws KeyException {
		/**
		* This function compare between all algorithms
		* @return  A Map of all algorithm combinations average solutions
		*/
		Map<String, List<Solution>> solutions = new HashMap<String, List<Solution>>();

		for(int i = 0; i < numBoards; i++) {
			Board board = Board.generateRandomBoard(boardSize, boardSize, numRandomMoves);
			Map<String, Solution> boardSolutions = runAllAlgorithms(board);
			
			for(Map.Entry<String, Solution> entry : boardSolutions.entrySet()){
				String algoCombination = entry.getKey();
				List<Solution> algoSolutions = null;

				if(solutions.keySet().contains(algoCombination))
				{
					algoSolutions = solutions.get(algoCombination);
				} else {
					algoSolutions = new ArrayList<Solution>();
				}
				algoSolutions.add(entry.getValue());
				solutions.put(algoCombination, algoSolutions);
			}
		}
		return getSolutionsAverage(solutions);
	}
	
	private static Map<String, Solution> getSolutionsAverage(Map<String, List<Solution>> solutions) {
		/**
		* This function calculate the average solution of each algorithm combination of algorithm and heuristic function
		* @return  A Map of solutions 
		*/

		Map<String, Solution> solutionsAverage = new HashMap<String, Solution>();
		long nanoDuration = 0;
		int nodeExplored = 0;
		int numOfSteps = 0;
		int numSolutions = 0;
		for(Map.Entry<String, List<Solution>> entry : solutions.entrySet()){
			for(Solution solution : entry.getValue()){
				nanoDuration += solution.getNanoDuration();
				nodeExplored += solution.getNodeExplored();
				numOfSteps += solution.getNumOfSteps();
				numSolutions += 1;
			}
			Solution averageSolution = new Solution();
			averageSolution.setNanoDuration(nanoDuration/numSolutions);
			averageSolution.setNodeExplored(nodeExplored/numSolutions);
			averageSolution.setNumOfSteps(numOfSteps/numSolutions);
			averageSolution.setMessage(String.format("Average solution of %s with %d boards",entry.getKey(), numSolutions));
			solutionsAverage.put(entry.getKey(), averageSolution);
			nanoDuration = 0;
			nodeExplored = 0;
			numOfSteps = 0;
			numSolutions = 0;
		}		
		return solutionsAverage;
	}
}
