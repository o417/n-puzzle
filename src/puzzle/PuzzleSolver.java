package puzzle;

import algorithms.Algorithm;
import algorithms.Solution;
import exceptions.KeyException;
import factories.AlgorithmFactory;
import factories.HeuristicFactory;
import game.Board;
import game.State;

public class PuzzleSolver {
	private static AlgorithmFactory algorithmFactory = new AlgorithmFactory();
	private static HeuristicFactory heuristicFactory = new HeuristicFactory();
	
	private final Algorithm algorithm;

	private PuzzleSolver(PuzzleSolverBuilder builder) throws KeyException {
		State initialState = new State(builder.getBoard(),null, heuristicFactory.GetNewProduct(builder.getHeuristicName()));
		this.algorithm = algorithmFactory.GetNewProduct(builder.getAlgorithmName());
		setAlgorithmProperties(initialState);
	}

	public void setAlgorithmProperties(State initialState) {
		State goalState = State.getGoalState(initialState);
		this.algorithm.setInitialState(initialState);
		this.algorithm.setGoalState(goalState);
	}

	public Solution solve() {
		return this.algorithm.solve();
	}
	
	@Override
	public String toString() {
		return String.format("Puzzle Solver\n%s", this.algorithm);
	}

	public static class PuzzleSolverBuilder {
		private static final int DEFAULT_BOARD_SIZE = 4;
		private static final int DEFAULT_NUM_RANDOM_MOVES = 10;
		private static final String DEFAULT_ALGORITHM_NAME = "AStar";
		private static final String DEFAULT_HEURISITC_NAME = "Zero";
		
		private Board board;
		private String algorithmName;
		private String heuristicName;

		public PuzzleSolverBuilder() {
			this.board = Board.generateRandomBoard(DEFAULT_BOARD_SIZE, DEFAULT_BOARD_SIZE, DEFAULT_NUM_RANDOM_MOVES);
			this.algorithmName = DEFAULT_ALGORITHM_NAME;
			this.heuristicName = DEFAULT_HEURISITC_NAME;
		}

		public PuzzleSolverBuilder setBoard(Board board) {
			this.board = board;
			return this;
		}

		public PuzzleSolverBuilder setAlgorithmName(String algorithmName) {
			this.algorithmName = algorithmName;
			return this;
		}

		public PuzzleSolverBuilder setHeuristicName(String heuristicName) {
			this.heuristicName = heuristicName;
			return this;
		}

		public PuzzleSolver build() throws KeyException {
			return new PuzzleSolver(this);
		}

		public Board getBoard() {
			return board;
		}

		public String getAlgorithmName() {
			return algorithmName;
		}

		public String getHeuristicName() {
			return heuristicName;
		}
	}
}
