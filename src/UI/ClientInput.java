package UI;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import game.Board;

public class ClientInput {

	public static Map<String, Integer> allowedValues;
 
	static {
		allowedValues = new HashMap<String, Integer>();
		allowedValues.put("MIN_BOARD_SIZE", 2);
		allowedValues.put("MIN_NUM_BOARDS", 1);
		allowedValues.put("MIN_NUM_RANDOM_MOVES", 0);
	}

	public static String inputClientChoice()
    {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Your choice: ");
        String choice = scanner.nextLine();
        return choice;
    }

    public static Board inputBoardByIndex(){
        int boardSize = inputBoardSize();
		List<Integer> boardValidValues = Board.getValidValues(boardSize, boardSize);
		int maxValidValue = Board.getMaxValidValue(boardSize, boardSize);
		int minValidValue = Board.getMinValidValue();
        int[][] board = new int[boardSize][boardSize];

		System.out.println("Board Values: " + boardValidValues);

        for(int i = 0; i < boardSize ; i++) {
        	 for(int j = 0; j < boardSize ; j++) {
				do{
					board[i][j] = inputInt(String.format("(%d, %d): ", i+1,j+1), minValidValue, maxValidValue );
					if(!boardValidValues.contains(board[i][j])){
						System.out.println("ERROR - the board already has this value");
					}
				}while(!boardValidValues.contains(board[i][j]));
				boardValidValues.remove(Integer.valueOf(board[i][j]));
				System.out.println("Last values: " + boardValidValues);
        	 }
        	 System.out.println();
        }
		//input.close();
        return new Board(board);
	}
    public static int inputBoardSize(){
		return inputInt("Enter the board size: ", allowedValues.get("MIN_BOARD_SIZE"));
	}

	public static int inputNumBoards(){
		return inputInt("Enter the number of boards: ", allowedValues.get("MIN_NUM_BOARDS"));
	}

	public static int inputNumRandMoves(){
		return inputInt("Enter the number of random moves: ", allowedValues.get("MIN_NUM_RANDOM_MOVES"));
	}

	public static int inputInt(String prompt, int minAllowedValue) {
        return inputInt(prompt, minAllowedValue, Integer.MAX_VALUE);
    }

	public static int inputInt(String message, int minAllowedValue, int maxAllowedValue ) {
		Scanner scanner = new Scanner(System.in);
		int clientInput = 0;
        boolean validInput = false;
		
		while (!validInput) {
            System.out.print(message);
            String input = scanner.nextLine();

            try {
                clientInput = Integer.parseInt(input);
				if(clientInput < minAllowedValue){
					System.out.println("ERROR - Invalid input. Enter an integer greater than or equal to " + minAllowedValue);
				}
				else if(clientInput > maxAllowedValue){
					System.out.println("ERROR - Invalid input. Enter an integer smaller than or equal to " + maxAllowedValue);
				}
				else{
					validInput = true;
				}
            } catch (NumberFormatException e) {
                System.out.println("ERROR - Invalid input. Enter an integer");
            }
        }
		
   	 	return clientInput;
	}
}
