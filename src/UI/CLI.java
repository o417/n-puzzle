package UI;

import java.util.Map;

import algorithms.Solution;
import exceptions.ChoiceException;
import exceptions.KeyException;
import game.Board;
import game.State;
import puzzle.Puzzle;

public class CLI implements IUI{
	public static final int DEFAULT_BOARD_SIZE = 4;
	public static final int DEFAULT_NUM_RANDOM_MOVES = 10;
	enum BoardCreationType{
		INPUTED, RAMDOM
	};
	int numRandomMoves = DEFAULT_NUM_RANDOM_MOVES;
	BoardCreationType boardCreationType = BoardCreationType.RAMDOM;
	Board gameBoard;
	boolean clientExit = false;

	 public CLI() {
		 this.gameBoard = Board.generateRandomBoard(DEFAULT_BOARD_SIZE, DEFAULT_BOARD_SIZE, DEFAULT_NUM_RANDOM_MOVES);
	 }
	 
	 public void start() {
		 while (!isClientExit()) {
			 try {
				 menu();
				 handleClient();
			 }
			 catch(ChoiceException e) {
				System.out.println(e.toString());
			}
			catch(KeyException e) {
				System.out.println(e.toString());
			}
			catch(IllegalStateException e) {
				System.out.println(e.toString());
			 } finally {}
		}
		exitMessage();
	 }

	
	private boolean isClientExit() {
		return this.clientExit;
	}


	@Override
	public void handleClient() throws ChoiceException, KeyException {
		String choice = ClientInput.inputClientChoice();
        System.out.println();
        switch (choice) {
		case "1": {
	        this.gameBoard = ClientInput.inputBoardByIndex();
			this.boardCreationType = BoardCreationType.INPUTED;
	        break;
		}
		case "2": {
			this.gameBoard = getRandomBoard();
			this.boardCreationType = BoardCreationType.RAMDOM;
		    break;
		}
		case "3": {
			System.out.println(this.gameBoard);
		    break;
		}
		case "4": {
			printFullSolution(Puzzle.runAlgorithm(this.gameBoard, "BFS", "Zero"));
			
		    break;
		}
		case "5": {
			printFullSolution(Puzzle.runAlgorithm(this.gameBoard, "AStar", "Zero"));
		    break;
		}
		case "6": {
			printFullSolution(Puzzle.runAlgorithm(this.gameBoard, "AStar", "Manhatten"));
		    break;
		}
		case "7": {
			printFullSolution(Puzzle.runAlgorithm(this.gameBoard, "AStar", "InadmissibleHeuristic"));
		    break;
		}
		case "8": {
			System.out.println(String.format("---------- Intial Board ----------\n%s", this.gameBoard));
			printSolutions(Puzzle.runAllAlgorithms(this.gameBoard));
		    break;
		}
		case "9": {
			printSolutions(runComparator());
			break;
		}
		case "10": {
			this.clientExit = true;
		    break;
		}
		default:
			throw new ChoiceException("No such option: " + choice);
		}
	}
	
	@Override
	public void menu() {
		String menu;
		String baseMenu = "**************** N PUZZLE *****************\n"
						+ "* Enter 1 -> INPUT BOARD                  *\n"
						+ "* Enter 2 -> RANDOM BOARD                 *\n"
						+ "* Enter 3 -> PRINT BOARD                  *\n"
				        + "* Enter 4 -> RUN BFS                      *\n"
				        + "* Enter 5 -> RUN DIJKSTRA                 *\n"
				        + "* Enter 6 -> RUN A* MANHATTEN             *\n"
						+ "* Enter 7 -> RUN A* INADMISSIBLE          *\n"
				        + "* Enter 8 -> RUN ALL ALGORITHMS           *\n"
				        + "* Enter 9 -> RUN COMPARATOR               *\n"
				        + "* Enter 10 -> TO EXIT                     *\n"
				        + "*                                         *\n";
		if(this.boardCreationType.equals(BoardCreationType.RAMDOM)){
			menu = String.format(
				"%s %s: board size [%d, %d] random moves %d\n"
			 + "*******************************************\n",
			 baseMenu, this.boardCreationType, this.gameBoard.getRows(),
			 this.gameBoard.getColumns(), this.numRandomMoves);
		}

		else{
			menu = String.format(
				"%s %s: board size [%d, %d]\n"
			 + "*******************************************\n",
			 baseMenu, this.boardCreationType, this.gameBoard.getRows(),
			 this.gameBoard.getColumns());
		}
				     
		
		System.out.println(menu);
	}
	
	public void exitMessage() {
		System.out.print("Hope this tool has helped you in your research");
	}
	
	private void printSolutions(Map<String, Solution> solutions) {
		System.out.println("---------- Solutions ----------");
		for(Map.Entry<String, Solution> solutionEntry : solutions.entrySet()) {
			System.out.println(String.format("----- %s -----\n%s\n", solutionEntry.getKey(), solutionEntry.getValue()));
		}
	}
	
	private void printFullSolution(Solution solution) {
		System.out.println(solution);
		System.out.println(String.format("---------- PATH ----------\n"
										+ "%s\n"
										+ "---------- END ----------",
		State.getPath(solution.getPath())));
	}

	private Board getRandomBoard() {
		int boardSize = ClientInput.inputBoardSize();
		int numRandomMoves = ClientInput.inputNumRandMoves();
		setNumRandomMoves(numRandomMoves);
		return Board.generateRandomBoard(boardSize, boardSize, numRandomMoves);
	}

	private void setNumRandomMoves(int numRandomMoves){
		this.numRandomMoves = numRandomMoves;
	}
	
	private Map<String, Solution> runComparator() throws KeyException{
		int boardSize = ClientInput.inputBoardSize();
		int numBoards = ClientInput.inputNumBoards();
		int numRandomMoves = ClientInput.inputNumRandMoves();

		return Puzzle.runComparator(boardSize, numBoards, numRandomMoves);
	}
}
