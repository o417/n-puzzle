package UI;

import exceptions.ChoiceException;
import exceptions.KeyException;

public interface IUI {
	void start();
    void menu();
    void handleClient() throws ChoiceException, KeyException;
}
