package heuristic;

import game.Board;
import game.State;

public class Hamming extends Heuristic{

    @Override
	public int getHeuristicValue(State state) {
	     int[] flattenBoard = state.getBoard().getFlattenBoard();
	     int misplacedTilesCounter = 0;
	     
	     for(int i = 0; i < flattenBoard.length; i ++) {
            if(flattenBoard[i] == Board.EMPTY_CELL_VALUE){
                continue;
            }
	    	 else if(flattenBoard[i] != i+1) {
                misplacedTilesCounter +=1;
	    	 }
	     }
	     return misplacedTilesCounter;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return new Hamming();
	}

	@Override
	public String toString() {
		return "Hamming";
	}
    
}
