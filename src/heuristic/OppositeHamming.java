package heuristic;

import game.State;

public class OppositeHamming extends Heuristic{

	@Override
	public int getHeuristicValue(State state) {
		 int numTiles = state.getBoard().getColumns() * state.getBoard().getRows()-1;
		 return numTiles - new Hamming().getHeuristicValue(state);
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return new OppositeHamming();
	}

	@Override
	public String toString() {
		return "OppositeHamming";
	}
}
