package heuristic;
import game.Board;
import game.Cell;
import game.State;

public class Manhatten extends Heuristic{

    @Override
    public int getHeuristicValue(State state) {
        int rows = state.getBoard().getRows();
        int columns = state.getBoard().getColumns();
        int[][] board = state.getBoard().getBoard();
        int distance = 0;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                int value = board[i][j];
                if (value != Board.EMPTY_CELL_VALUE) {
                    Cell goalPosition = Board.getValueGoalPosition(value, rows, columns);
                    Cell currentPosition = new Cell(i,j);
                    distance += Manhatten.getManhattenDistance(currentPosition, goalPosition);
                }
            }
        }

        return distance;
    }

    public static int getManhattenDistance(Cell currentPosition, Cell goalPosition){
        return getManhattenDistance(currentPosition.getX(), currentPosition.getY(),
                goalPosition.getX(), goalPosition.getY());
    }

    public static int getManhattenDistance(int currentX, int currentY, int goalX, int goalY) {
        return Math.abs(goalX - currentX) + Math.abs(goalY - currentY);
    }
    

	@Override
	public Object clone() throws CloneNotSupportedException {
		return new Manhatten();
	}

	@Override
	public String toString() {
		return "Manhatten";
	}
}
