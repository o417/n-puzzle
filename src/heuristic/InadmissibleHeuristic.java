package heuristic;

import game.Board;
import game.Cell;
import game.State;

public class InadmissibleHeuristic extends Heuristic{

    @Override
    public int getHeuristicValue(State state) {
        int rows = state.getBoard().getRows();
        int columns = state.getBoard().getColumns();
        int[][] board = state.getBoard().getBoard();
        int heuristicValue = 0;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                int value = board[i][j];

                if(value == Board.EMPTY_CELL_VALUE){
                    continue;
                }

                else{ 
                    Cell goalPosition = Board.getValueGoalPosition(value, rows, columns);
                    Cell currentPosition = new Cell(i,j);
                    int manhattan = 1 + (int) Manhatten.getManhattenDistance(currentPosition, goalPosition)/2;
                    int randomMultiplier = 1 + (int) (Math.random() * 5);
                    heuristicValue += manhattan * randomMultiplier;
                }
            }
        }
        return heuristicValue;
    }

	@Override
	public Object clone() throws CloneNotSupportedException {
		return new InadmissibleHeuristic();
	}

	@Override
	public String toString() {
		return "InadmissibleHeuristic";
	}
}
