package heuristic;

import game.State;

public abstract class Heuristic implements Cloneable{
	public abstract int getHeuristicValue(State state);
    public abstract Object clone() throws CloneNotSupportedException;
    public abstract String toString();
}

