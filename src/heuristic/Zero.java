package heuristic;

import game.State;

public class Zero extends Heuristic{

	@Override
	public int getHeuristicValue(State state) {
		return 0;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return new Zero();
	}

	@Override
	public String toString() {
		return "Dijkstra version";
	}

}
