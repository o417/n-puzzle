# N Puzzle

This project implements a CLI tool for solving n puzzle game, based on the well known puzzle 15 game.<br/><br/>
Using this CLI tool you can input or generate random puzzles bases on puzzle 15, and solve thes using diffrent algorithms.

## Features

### Input or generate random puzzles with n side size
* Input board cell by cell
* Generate the board by applying an inputted number of random moves from the solution board.

### Run one of these algorithms or all of them
Each run of an algorithm will display the nano duration time, the number of explored nodes, the number of steps to the solution and, the solution itself.<br/>
* BFS
* Dijkstra 
* A* with Manhatten admissible heuristic function
* A* with inadmissible heuristic function

### Compare all of these algorithms
This feature compares all the algorithms in terms of the nano duration time, the number of explored nodes, and the number of steps to the solution.<br/>
* Input board size side 
* Input number of boards
* Input the number of random moves from the solution board

## Repository overview

The project has a UI package that represents the User Interface layer.<br/>
All the other packages combined represent the Business Logic layer.

├── .gitignore<br/>
├── README.md<br/>
├── src<br/>
&emsp;&emsp;├── algorithms<br/>
&emsp;&emsp;├── exceptions<br/>
&emsp;&emsp;├── factories<br/>
&emsp;&emsp;├── game<br/>
&emsp;&emsp;├── heuristic<br/>
&emsp;&emsp;├── puzzle<br/>
&emsp;&emsp;├── UI<br/>